# azul-mirrors

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Install azul mirrorlist

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/configuration/azul-mirrors.git
```
<br><br>


# Mirror list:

**azul mirrorlist from 2023.06.27 (file: 20230627-1)**

# Mirror 01:
Server = https://ftp.jaist.ac.jp/pub/sourceforge.jp/storage/g/a/az/azul/

# Mirror 02:
Server = https://ftp.iij.ad.jp/pub/osdn.jp/storage/g/a/az/azul/

# Mirror 03:
Server = https://free.nchc.org.tw/osdn/storage/g/a/az/azul/

# Mirror 04-1:
Server = https://ftp.halifax.rwth-aachen.de/osdn/storage/g/a/az/azul/

# Mirror 04-2:
Server = http://ftp.halifax.rwth-aachen.de/osdn/storage/g/a/az/azul/

# Mirror 05:
Server = https://openbsd.c3sl.ufpr.br/osdn/storage/g/a/az/azul/

# Mirror 06:
Server = https://ftp.sunet.se/mirror/osdn.net/storage/g/a/az/azul/

# Mirror 07:
Server = http://pumath.dl.osdn.jp/storage/g/a/az/azul/

# Mirror 08:
Server = https://mirror.accum.se/mirror/osdn.net/storage/g/a/az/azul/

# Mirror 09:
Server = https://mirrors.xie.ke/osdn/storage/g/a/az/azul/

# Mirror 10-1:
Server = https://mirror.math.princeton.edu/pub/osdn/storage/g/a/az/azul/

# Mirror 10-2:
Server = http://mirror.math.princeton.edu/pub/osdn/storage/g/a/az/azul/

# Mirror 11:
Server = http://194.71.11.163/mirror/osdn.net/storage/g/a/az/azul/

# Mirror 12:
Server = https://ftp.acc.umu.se/mirror/osdn.net/storage/g/a/az/azul/

# Mirror 13:
Server = https://ftp.halifax.rwth-aachen.de/osdn/storage/g/a/az/azul/

# Mirror 14:
Server = https://mirrors.ocf.berkeley.edu/osdn/storage/g/a/az/azul/

# Mirror 15:
Server = https://plug-mirror.rcac.purdue.edu/osdn/storage/g/a/az/azul/

# Mirror 16:
Server = https://mirror.liquidtelecom.com/osdn/storage/g/a/az/azul/

# Mirror 17:
Server = http://osdn.mirror.constant.com/storage/g/a/az/azul/

########################################################
# Gobal Mirrors                                        #
########################################################

# OSDN Global Mirror 01:
Server = https://ja.osdn.net/projects/azul/storage/

# OSDN Global Mirror 02:
Server = https://zh.osdn.net/projects/azul/storage/

# OSDN Global Mirror 03:
Server = https://osdn.net/projects/azul/storage/

# SourceForge Global Mirror 04:
Server = https://sourceforge.net/projects/azul-repo/files/




#################### Mirrors with errors ####################

########################################################
# Mirrors not updated at the moment                    #
########################################################

# Mirror 001:
Server = https://mirrors.dotsrc.org/osdn/storage/g/a/az/azul/

# Mirror 002:
Server = http://ftp.dk.xemacs.org/osdn/storage/g/a/az/azul/

# Mirror 003-1:
Server = https://ftp.openbsd.dk/osdn/storage/g/a/az/azul/

# Mirror 003-2:
Server = http://ftp.openbsd.dk/osdn/storage/g/a/az/azul/

# Mirror 004:
Server = http://mirroronet.pl/pub/mirrors/sourceforge.jp/storage/g/a/az/azul/




########################################################
# Mirrors with old versions                            #
########################################################

# Mirror 01-1:
Server = http://mirrors.gigenet.com/OSDN/storage/g/a/az/azul/

# Mirror 01-2:
Server = https://mirrors.gigenet.com/OSDN/storage/g/a/az/azul/






########################################################
# Mirrors offline:                                     #
########################################################

# Mirror 13-1:
Server = http://osdn.ip-connect.vn.ua/storage/g/a/az/azul/

# Mirror 13-2:
Server = https://osdn.ip-connect.vn.ua/storage/g/a/az/azul/




########################################################
# Mirrors with errors:                                 #
########################################################

# The folder does not currently exist:
# Server = https://mirrors.tuna.tsinghua.edu.cn/osdn/storage/g/a/az/azul/

## <<== End mirrorlist file ==>> ##
